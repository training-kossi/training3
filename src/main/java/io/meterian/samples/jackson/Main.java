package io.meterian.samples.jackson;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

class Vertex {
	String label;
	Vertex(String label) {
		this.label = label;
	}

	// equals and hashCode
}

class Graph<T, V> {
	private Map<T, V> adjVertices;

	// standard constructor, getters, setters

	void addVertex(String label) {
		adjVertices.putIfAbsent(null, null);
	}

	void removeVertex(String label) {
		Vertex v = new Vertex(label);
		adjVertices.values().stream().forEach(e -> ((Map<T, V>) e).remove(v));
		adjVertices.remove(new Vertex(label));
	}

	
	void addEdge(String label1, String label2) {
		Vertex v1 = new Vertex(label1);
		Vertex v2 = new Vertex(label2);
		((Collection<V>) adjVertices.get(v1)).add((V) v2);
		((Collection<V>) adjVertices.get(v2)).add((V) v1);
	}

	void removeEdge(String label1, String label2) {
		Vertex v1 = new Vertex(label1);
		Vertex v2 = new Vertex(label2);
		List<Vertex> eV1 = (List<Vertex>) adjVertices.get(v1);
		List<Vertex> eV2 = (List<Vertex>) adjVertices.get(v2);
		if (eV1 != null)
			eV1.remove(v2);
		if (eV2 != null)
			eV2.remove(v1);
	}

	Graph createGraph() {
		Graph graph = new Graph();
		graph.addVertex("Bob");
		graph.addVertex("Alice");
		graph.addVertex("Mark");
		graph.addVertex("Rob");
		graph.addVertex("Maria");
		graph.addEdge("Bob", "Alice");
		graph.addEdge("Bob", "Rob");
		graph.addEdge("Alice", "Mark");
		graph.addEdge("Rob", "Mark");
		graph.addEdge("Alice", "Maria");
		graph.addEdge("Rob", "Maria");
		return graph;
	}

	List getAdjVertices(String label) {
		return (List) adjVertices.get(new Vertex(label));
	}

	Set<String> depthFirstTraversal(Graph graph, String root) {
		Set<String> visited = new LinkedHashSet<String>();
		Stack<String> stack = new Stack<String>();
		stack.push(root);
		while (!stack.isEmpty()) {
			String vertex = stack.pop();
			if (!visited.contains(vertex)) {
				visited.add(vertex);
				for (Object v : graph.getAdjVertices(vertex)) {              
					//stack.push((Vertex)v.label);
				}
			}
		}
		return visited;
	}

	Set<String> breadthFirstTraversal(Graph graph, String root) {
		Set<String> visited = new LinkedHashSet<String>();
		Queue<String> queue = new LinkedList<String>();
		queue.add(root);
		visited.add(root);
		while (!queue.isEmpty()) {
			String vertex = queue.poll();
			for (Object v : graph.getAdjVertices(vertex)) {
				Vertex v1 = (Vertex) v;
				if (!visited.contains(v1.label)) {
					visited.add(v1.label);
					queue.add(v1.label);
           System.out.println("This is good for you");  
				}
			}
		}
		return visited;
	}    

}